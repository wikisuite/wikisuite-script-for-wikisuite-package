#!/usr/bin/env bash
# Check if the script is run as root
if [ "$EUID" -ne 0 ]; then
  echo "Error: This script must be run as root." >&2
  exit 1
fi
#
echo
echo '* Updating the package repository information and allow releaseinfo change'
echo
apt-get --allow-releaseinfo-change update
echo
echo '* Updating the package repository information'
echo
apt-get update
echo
echo '* Installing sudo'
echo
apt-get install sudo
echo
echo '* Updating the package repository information'
echo
sudo apt-get update && sudo apt-get upgrade
echo
echo '* Installing curl'
echo
sudo apt-get install curl
echo
host=$1
if [ -z "$1" ]
    then
        host=$(hostname -f)
fi
echo '* Set hostname' $host
echo
sudo hostnamectl set-hostname $host
echo
echo '* Configuring automated backups to S3 bucket'
# Ask if the user wants to set the S3 backups credentials in y/N
read -p "Do you want to configure automated backups to S3 bucket? (y/N) " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
    echo '* Setting S3 backup credentials'
    prompt_for_input() {
        local var_name="$1"
        local prompt_message="$2"
        local mandatory="$3"
        while [ -z "${!var_name}" ]; do
            read -p "  - $prompt_message: " input_value
            if [ -z "$input_value" ] && [ "$mandatory" != "mandatory" ]; then
                break
            fi
            export $var_name="$input_value"
        done
    }
    prompt_for_input "S3KEY" "Set S3 Access Key ID" "mandatory"
    prompt_for_input "S3SEC" "Set S3 Secret Access Key" "mandatory"
    prompt_for_input "S3RGN" "Set S3 Region" "mandatory"
    prompt_for_input "S3END" "Set S3 Endpoint [default: 's3.amazonaws.com']"
    prompt_for_input "S3NAM" "Set S3 Backup Name [default: 'Default S3 Backup Server']"
fi
echo
echo '* Downloading wikisuite installer'
echo
curl -o wikisuite-installer https://gitlab.com/wikisuite/wikisuite-packages/-/raw/main/wikisuite-installer
echo
echo '* Execute wikisuite-installer bash'
. ./wikisuite-installer
echo
