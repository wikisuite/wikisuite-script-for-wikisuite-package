# WikiSuite Script For WikiSuite Package
Script for quick installation for WikiSuite package.

## Getting started

Download the file from WikiSuite repos
```shell``
wget -O wikisuite-script-install.sh https://gitlab.com/wikisuite/wikisuite-script-for-wikisuite-package/-/raw/main/install.sh
```
Then you can run the script
```shell
# execute the script
sh wikisuite-script-install.sh 
```

The host name is set automatically. In case you want to update the host name manually, check your host name with the command :
```shell
hostnamectl
```
Replace 'machinename.example.org' with your host name.
```shell
sh wikisuite-script-install.sh machinename.example.org
```

## Note
This script has been set to be used only for virtualmin installation.
For more detail go to [How to install WikiSuite](https://wikisuite.org/How-to-install-WikiSuite)
